package models;

public class Monom {
	private double coef;
	private int grd;
	
	public Monom(double coef , int grd){
		this.coef = coef;
		this.grd = grd;
	}

	public double getCoef() {
		return coef;
	}

	public int getGrd() {
		return grd;
	}
	public void setCoef(double coef) {
		this.coef = coef;
	}
	@Override
	public String toString(){
		if (grd == 0)
			return String.format("%.2f", coef);
		else 
			if (coef == 1){
				if (grd == 1)
					return "x";
				else
					return String.format("x^%d", grd);
			}
			else{
				if (coef == -1)
					if (grd == 1)
						return "-x";
					else
						return String.format("-x^%d" , grd);
				else{
					if (grd == 1)
						return String.format("%.2fx", coef);
					else
						return String.format("%.2fx^%d", coef , grd);
				}
			}
			
	}
	
}

package models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Polinom {
	private List<Monom> term;
	
	public Polinom(){
		term = new ArrayList<Monom>();
	}
	public Polinom(String pol){
		term = new ArrayList<Monom>();
		String input = pol;
		//patternul corespunzator unui polinom cu coeficienti reali
		Pattern p = Pattern.compile("([+-]?\\d*\\.?\\d*)[xX](\\^(\\d+))?|([+-]?\\d+\\.?\\d*)" );
		Matcher m = p.matcher(input);
		while (m.find()) {
			double coef;
			int power;
			if (m.group(4)==null){
				//daca cea de'a 4a paranteza din pattern e diferita de null , gradul monomului este minim 1
				if (m.group(3) == null)
					power = 1;//x,2x etc
				else
					//daca cea de'a 3a paranteza e diferita de null , gradul monomului e minim 2
					power = Integer.parseInt(m.group(3));
				if (m.group(1).equals(""))
					coef = 1;//in cazul in care primul element are coeficient +1 , nu se scrie nimic inainte de x
				else
					if (m.group(1).equals("-"))
						coef = -1;//-x,-x^2
					else
						if (m.group(1).equals("+"))
						coef = 1;//+x,+x^2 termenii cu coeficient 1 , dar care nu sunt primul element
						else
							coef = Double.parseDouble(m.group(1));
			}
			else{//termenul liber
				coef = Double.parseDouble(m.group(4));
				power = 0;
			}
			
			
		    term.add(new Monom(coef , power));

		}
		sortByGrade();
	}

	public List<Monom> getTerm() {
		return term;
	}
	public void sortByGrade(){
		Collections.sort(term , new CompareByGrade());
	}
	//se foloseste pentru afisarea corecta a polinomului
	public String toString(){
		String s="";
		for (Monom helper : term){
			if(helper.getCoef()>0 )
				s+="+";
			s+=helper;
		}
		if (s.charAt(0) == '+')
			s = s.substring(1, s.length());
		return s;
	}
}

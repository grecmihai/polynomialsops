package models;

import java.util.Comparator;

public class CompareByGrade implements Comparator<Monom>{
	@Override
	public int compare(Monom m1 , Monom m2){
		int result;
		result = m2.getGrd() - m1.getGrd();
		return result;
		
	}
}

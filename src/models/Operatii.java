package models;

import java.util.ArrayList;
import java.util.List;

public class Operatii {
	//operatiile clasice pe monoame
	public static Monom addMonom(Monom a , Monom b){
		return new Monom(a.getCoef() + b.getCoef(),a.getGrd());
	}
	
	public static Monom subMonom(Monom a , Monom b){
		return new Monom(a.getCoef() - b.getCoef() , a.getGrd());
	}
	
	public static Monom inmMonom(Monom a , Monom b){
		return new Monom(a.getCoef() * b.getCoef() , a.getGrd() + b.getGrd());
	}
	
	public static Monom impMonom(Monom a , Monom b){
		return new Monom(a.getCoef() / b.getCoef() , a.getGrd() - b.getGrd());
	}
	
	public static Polinom add(Polinom a , Polinom b){
		Polinom result = new Polinom();
		List<Monom> trash = new ArrayList<Monom>();//trash va contine termenii ce vor avea coeficientul 0 in urma adunarii
		//prima data incarcam in rezultat unul dintre polinoame
		for (Monom helper : a.getTerm())
			result.getTerm().add(helper);
		for (Monom helper : b.getTerm()){
			int existenta = 0;//folosim ca variabila booleana pentru a testa daca exista in rezultat monom cu gradul lui helper
			for (Monom terms : result.getTerm())
				if (helper.getGrd() == terms.getGrd()){
					//adunam monoamele cu acelasi grad
					Monom aux = addMonom(helper , terms);
					result.getTerm().set(result.getTerm().indexOf(terms), aux);//actualizam monomul din rezultat cu grad coresp
					existenta = 1;
					if (aux.getCoef() == 0 )
						trash.add(aux);
				}
			if (existenta == 0)//daca gradul nu exista , atunci doar adaugam monomul din al doilea polinom
				result.getTerm().add(helper);
		}
		result.getTerm().removeAll(trash);//eliminam toti termenii care sunt zero
		//in cazul in care rezultatul este nul , adaugam un monom cu coeficient si grad 0 pentru afisare
		if (result.getTerm().isEmpty())
			result.getTerm().add(new Monom(0,0));
		result.sortByGrade();//la final , sortam descrescator monoamele in functie de grad
		return result;
		}
	
	public static Polinom sub(Polinom a , Polinom b){
		Polinom result = new Polinom();
		List<Monom> trash = new ArrayList<Monom>();//trash va contine elementele cu coeficient 0 in urma executarii operatiilor
		for (Monom helper : a.getTerm())
			result.getTerm().add(helper);//adaugam deimpartitul
		for (Monom helper : b.getTerm()){
			int existenta = 0;//folosim pentru a verifica daca monoamele din impartitor au corespondent in grad in polinomul rezultat
			for (Monom terms : result.getTerm())
				if (helper.getGrd() == terms.getGrd()){
					Monom aux = subMonom(terms , helper);
					result.getTerm().set(result.getTerm().indexOf(terms), aux);
					existenta = 1;
					if (aux.getCoef() == 0 )
						trash.add(aux);
				}
			if (existenta == 0){
				//in caz contrar , monomul se introduce in polinom , cu schimbarea coeficientului in prealabil
				double aux = helper.getCoef()*(-1);
				helper.setCoef(aux);
				result.getTerm().add(helper);
			}
		}
		result.getTerm().removeAll(trash);//se scot toti termenii cu 0
		//daca rezultatul nu contine niciun termen , se adauga un termen "zero" pentru afisare
		if (result.getTerm().isEmpty())
			result.getTerm().add(new Monom(0,0));
		result.sortByGrade();//la final , rezutlatul este sortat descrescator dupa grad
		return result;
	}
	
	public static Polinom derivare(Polinom a){
		Polinom result = new Polinom();
		//tratam cazul in care polinom este alcatuit doar dintr'o constanta
		if (a.getTerm().get(0).getGrd() == 0){
			result.getTerm().add(new Monom(0,0));
			return result;
		}
		for (Monom helper : a.getTerm())
			if (helper.getGrd() > 0)//constanta dispare la derivare , de aceea aceasta nu este introdusa in rezultat
				result.getTerm().add(helper);
		for (Monom helper : result.getTerm())
			//fiecare termen este actualizat imediat , prin formula specifica derivarii monoamelor
				result.getTerm().set(result.getTerm().indexOf(helper), new Monom(helper.getCoef()*helper.getGrd() , helper.getGrd() - 1));
		return result;
	}
	
	public static Polinom integrare(Polinom a){
		Polinom result = new Polinom();
		for (Monom helper : a.getTerm())
			result.getTerm().add(helper);//se introduc toti termenii in polinomul rezultat
		for (Monom helper : result.getTerm())
			//termenii sunt actualizati imediat , folosind formula specifica integrari monoamelor
				result.getTerm().set(result.getTerm().indexOf(helper), new Monom(helper.getCoef()/(helper.getGrd() + 1) , helper.getGrd() + 1));
		return result;
	}
	
	public static Polinom inmultire(Polinom a , Polinom b){
		Polinom result = new Polinom();
		//se realizeaza inmultirea monoamelor termen cu termen
		for (Monom helperA : a.getTerm())
			for (Monom helperB : b.getTerm())
				result.getTerm().add(inmMonom(helperA , helperB));
		//in cazul in care in urma adunarii a 2 monoame cu acelasi grad , coeficientul rezultat este zero , atunci monomul 
		//este introdus in trash
		//de asemenea , in trash se pune operandul adunarilor in care nu se salveaza rezultatul
		List<Monom> trash = new ArrayList<Monom>();
		//cautam perechile de monoame cu acelasi grad
		for (int i = 0; i < result.getTerm().size() -1 ; i++)
			for (int j = i+1 ; j < result.getTerm().size() ; j++)
				if (result.getTerm().get(i).getGrd() == result.getTerm().get(j).getGrd()){
					//actualizam in primul monom gasit rezultatul
					result.getTerm().set(i, addMonom(result.getTerm().get(i) , result.getTerm().get(j)));
					trash.add(result.getTerm().get(j));//plasam celalalt monom pentru stergere
				}
		//parcurgem inca o data rezultatul pentru a gasi monoamele cu coeficient 0
		for (Monom helper : result.getTerm())
			if (helper.getCoef() == 0)
				trash.add(helper);
		result.getTerm().removeAll(trash);//eliminam monoamele necorespunzatoare
		result.sortByGrade();//sortam rezultatul descrescator in functie de grad
		
		return result;
	}
	
	public static List<Polinom> impartire(Polinom a , Polinom b){
		List<Polinom> catRest = new ArrayList<Polinom>();
		Monom catTerm;//in catTerm tinem rezultatul impartirii dintre monoamele maxime , conform algoritmului matematic
		Polinom aux;//in aux se tine rezultatul inmultirii dintre catTerm si impartitor
		Polinom helper = new Polinom();//helper este utilizat pentru a stoca catTerm si a folosi metoda de inmultire a 2 polinoame
		Polinom cat = new Polinom();//catul rezultat
		//verificam cazul in care impartitorul este un scalar
		if (b.getTerm().get(0).getGrd() == 0){
			Polinom rezultat = new Polinom();
			Polinom re = new Polinom();
			re.getTerm().add(new Monom(0,0));//la impartirea cu un scalar , restul este 0
			for (Monom help : a.getTerm())
				//introducem in rezultat rezultatul impartirii fiecarui monom din deimpartit la acel scalar
				rezultat.getTerm().add(new Monom(help.getCoef()/b.getTerm().get(0).getCoef() , help.getGrd()));
			catRest.add(rezultat);
			catRest.add(re);
			return catRest;//returnam un array de polinoame ce contine catul si restul
		}
		//tratam cazul in care impartitorul este "mai mare" decat deimpartitul(ca grad)
		//in acest caz , catul este 0 , iar restul este reprezentat de deimpartit
		if (a.getTerm().get(0).getGrd() < b.getTerm().get(0).getGrd()){
			Polinom zero = new Polinom();
			zero.getTerm().add(new Monom(0,0));
			catRest.add(zero);
			catRest.add(a);
		}
		//algoritmul normal se opreste in momentul in care gradul deimpartitului devine mai mic decat cel al impartitorului
		while(a.getTerm().get(0).getGrd() >= b.getTerm().get(0).getGrd()){
			catTerm = impMonom(a.getTerm().get(0),b.getTerm().get(0));
			helper.getTerm().add(catTerm);
			cat.getTerm().add(catTerm);//catul final este reprezentat de suma rezultatelor impartirii monoamelor maxime din D si I
			aux = inmultire(b , helper);
			helper.getTerm().clear();
			a = sub(a , aux);
		}
		//cazul in care restul e 0 , a ar fi gol , de aceea adaugam un monom 0
		if (a.getTerm().isEmpty())
			a.getTerm().add(new Monom(0,0));
		catRest.add(cat);
		catRest.add(a);//la finalul algoritmului , polinomul initial este modificat astfel incat devine restul impartirii
		return catRest;
	}
}


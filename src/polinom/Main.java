package polinom;

import controller.PolinoameController;
import views.PolinoameView;

public class Main {

	public static void main(String[] args) {
		PolinoameView view = new PolinoameView();
		PolinoameController controller = new PolinoameController(view);
		view.getMenuPage().setVisible(true);
	}

}

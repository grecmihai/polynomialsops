package views;

import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Menu extends JFrame{
	private JPanel panel;
	private JButton ad , sc , inm , imp , der , inte;
	
	public Menu(){
		super("Polinoame");
		panel = new JPanel();
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setContentPane(panel);
		this.setSize(640, 480);
		this.setLocation(600, 300);
		panel.setLayout(new GridLayout(3,3));
		
		ad = new JButton("Adunare");
		sc = new JButton("Scadere");
		inm = new JButton("Inmultire");
		imp = new JButton("Impartire");
		der = new JButton("Derivare");
		inte = new JButton("Integrare");
		
		panel.add(ad);
		panel.add(sc);
		panel.add(inm);
		panel.add(imp);
		panel.add(der);
		panel.add(inte);
		
		ad.setFont(new Font(ad.getFont().getName() , Font.BOLD  , ad.getFont().getSize() + 20));
		sc.setFont(new Font(ad.getFont().getName() , Font.BOLD  , sc.getFont().getSize() + 20));
		inm.setFont(new Font(ad.getFont().getName() , Font.BOLD  , inm.getFont().getSize() + 20));
		imp.setFont(new Font(ad.getFont().getName() , Font.BOLD  , imp.getFont().getSize() + 20));
		der.setFont(new Font(ad.getFont().getName() , Font.BOLD  , der.getFont().getSize() + 20));
		inte.setFont(new Font(ad.getFont().getName() , Font.BOLD  , inte.getFont().getSize() + 20));
	}
	
	public void addAdListener(ActionListener listener){
		ad.addActionListener(listener);
	}
	public void addScListener(ActionListener listener){
		sc.addActionListener(listener);
	}
	public void addInmListener(ActionListener listener){
		inm.addActionListener(listener);
	}
	public void addImpListener(ActionListener listener){
		imp.addActionListener(listener);
	}
	public void addDerListener(ActionListener listener){
		der.addActionListener(listener);
	}
	public void addInteListener(ActionListener listener){
		inte.addActionListener(listener);
	}

	public JButton getAd() {
		return ad;
	}

	public JButton getSc() {
		return sc;
	}

	public JButton getInm() {
		return inm;
	}

	public JButton getImp() {
		return imp;
	}

	public JButton getDer() {
		return der;
	}

	public JButton getInte() {
		return inte;
	}
	

}

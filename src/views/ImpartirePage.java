package views;

import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class ImpartirePage extends JFrame{
	private JButton bImp;
	private JPanel panel;
	private JTextField pol1;
	private JTextField pol2;
	private JLabel cat , rest;
	private JLabel rezultat1 , rezultat2;
	private JLabel p1 , p2;
	private JButton back;

	public ImpartirePage(){
		super("Impartire");
		setSize(630,400);
		setLocation(800,300);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		bImp = new JButton("Imparte");
		panel = new JPanel();
		pol1 = new JTextField(50);
		pol2 = new JTextField(50);
		cat = new JLabel("p1/p2=");
		rest = new JLabel("p1%p2=");
		p1 = new JLabel("p1:");
		p2 = new JLabel("p2:");
		cat.setFont(new Font(cat.getFont().getName() , Font.BOLD , cat.getFont().getSize() + 5));
		rest.setFont(new Font(rest.getFont().getName() , Font.BOLD , rest.getFont().getSize() + 5));
		back = new JButton("BACK");
		rezultat1 = new JLabel();
		rezultat2 = new JLabel();
		
		panel.setLayout (null); 


		pol1.setBounds(130,80,350,30);
		pol2.setBounds(130,150,350,30);
		p1.setBounds(80 , 80 , 30 , 30);
		p2.setBounds(80 , 150 , 30 , 30);
		bImp.setBounds(250,200,80,20);
		cat.setBounds(50 , 250 , 80 , 20);
		rezultat1.setBounds(120, 250 , 500 , 20);
		rest.setBounds(50 , 300 , 80 , 20);
		rezultat2.setBounds(120, 300 , 500 , 20);
		back.setBounds(20 , 20 , 75 , 20);
		
		panel.add(back);
		panel.add(bImp);
		panel.add(pol1);
		panel.add(p1);
		panel.add(p2);
		panel.add(pol2);
		panel.add(cat);
		panel.add(rest);
		panel.add(rezultat1);
		panel.add(rezultat2);

		getContentPane().add(panel);
	}
	
	public void addBImpListener(ActionListener listener){
		bImp.addActionListener(listener);
	}
	public void addBackListener(ActionListener listener){
		back.addActionListener(listener);
	}

	public JTextField getPol1() {
		return pol1;
	}

	public JTextField getPol2() {
		return pol2;
	}

	public JLabel getRezultat1() {
		return rezultat1;
	}

	public JLabel getRezultat2() {
		return rezultat2;
	}
	
	
	
	
	
}

package views;

public class PolinoameView {
	private AdunarePage adunarePage;
	private DerivarePage derivarePage;
	private ImpartirePage impartirePage;
	private InmultirePage inmultirePage;
	private IntegrarePage integrarePage;
	private Menu menuPage;
	private ScaderePage scaderePage;
	
	public PolinoameView(){
		adunarePage = new AdunarePage();
		derivarePage = new DerivarePage();
		impartirePage = new ImpartirePage();
		inmultirePage = new InmultirePage();
		integrarePage = new IntegrarePage();
		menuPage = new Menu();
		scaderePage = new ScaderePage();
	}

	public AdunarePage getAdunarePage() {
		return adunarePage;
	}

	public DerivarePage getDerivarePage() {
		return derivarePage;
	}

	public ImpartirePage getImpartirePage() {
		return impartirePage;
	}

	public InmultirePage getInmultirePage() {
		return inmultirePage;
	}

	public IntegrarePage getIntegrarePage() {
		return integrarePage;
	}

	public Menu getMenuPage() {
		return menuPage;
	}

	public ScaderePage getScaderePage() {
		return scaderePage;
	}
	
}

package views;

import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class AdunarePage extends JFrame{
	private JButton bAdd;
	private JPanel panel;
	private JTextField pol1;
	private JTextField pol2;
	private JLabel rezultat;
	private JLabel p1 , p2;
	private JButton back;
	private JLabel rezultat2;

	public AdunarePage(){
		super("Adunare");
		setSize(630,400);
		setLocation(800,300);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		bAdd = new JButton("Aduna");
		panel = new JPanel();
		pol1 = new JTextField(30);
		pol2 = new JTextField(30);
		rezultat = new JLabel("p1+p2=");
		rezultat2 = new JLabel();
		p1 = new JLabel("p1:");
		p2 = new JLabel("p2:");
		rezultat.setFont(new Font(rezultat.getFont().getName() , Font.BOLD , rezultat.getFont().getSize() + 5));
		rezultat2.setFont(new Font(rezultat.getFont().getName() , Font.BOLD , rezultat.getFont().getSize() + 5));
		back = new JButton("BACK");
		
		
		panel.setLayout (null); 

		pol1.setBounds(130,80,350,30);
		pol2.setBounds(130,150,350,30);
		p1.setBounds(80 , 80 , 30 , 30);
		p2.setBounds(80 , 150 , 30 , 30);
		bAdd.setBounds(250,200,80,20);
		rezultat.setBounds(50 , 250 , 80 , 20);
		rezultat2.setBounds(120, 250 , 500 , 20);
		back.setBounds(20 , 20 , 75 , 20);

		panel.add(back);
		panel.add(bAdd);
		panel.add(pol1);
		panel.add(p1);
		panel.add(p2);
		panel.add(pol2);
		panel.add(rezultat);
		panel.add(rezultat2);

		getContentPane().add(panel);
	}
	
	public void addBAddListener(ActionListener listener){
		bAdd.addActionListener(listener);
	}
	public void addBackListener(ActionListener listener){
		back.addActionListener(listener);
	}
	public JTextField getPol1() {
		return pol1;
	}

	public JTextField getPol2() {
		return pol2;
	}

	public JLabel getRezultat2() {
		return rezultat2;
	}
	
	

	
	
	
	
	
	
}

package views;

import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class IntegrarePage extends JFrame{
	private JButton bInt;
	private JPanel panel;
	private JTextField pol1;
	private JLabel rezultat,rezultat2;
	private JLabel p ;
	private JButton back;

	public IntegrarePage(){
		super("Integrare");
		setSize(630,300);
		setLocation(800,300);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		bInt = new JButton("Integreaza");
		panel = new JPanel();
		pol1 = new JTextField(50);
		rezultat = new JLabel("dp/dx=");
		rezultat2 = new JLabel();
		p = new JLabel("p:");
		rezultat.setFont(new Font(rezultat.getFont().getName() , Font.BOLD , rezultat.getFont().getSize() + 5));
		rezultat2.setFont(new Font(rezultat.getFont().getName() , Font.BOLD , rezultat.getFont().getSize() + 5));
		back = new JButton("BACK");
		
		
		panel.setLayout (null); 


		pol1.setBounds(130,80,300,30);
		p.setBounds(80 , 80 , 30 , 30);
		bInt.setBounds(250,150,120,20);
		rezultat.setBounds(50 , 200 , 80 , 20);
		rezultat2.setBounds(120, 200 , 500 , 20);
		back.setBounds(20 , 20 , 75 , 20);


		panel.add(bInt);
		panel.add(pol1);
		panel.add(p);
		panel.add(rezultat);
		panel.add(back);
		panel.add(rezultat2);

		getContentPane().add(panel);
	}
	
	public void addBIntListener(ActionListener listener){
		bInt.addActionListener(listener);
	}
	public void addBackListener(ActionListener listener){
		back.addActionListener(listener);
	}

	public JLabel getRezultat2() {
		return rezultat2;
	}

	public JTextField getPol1() {
		return pol1;
	}
	
	
	
	
	
}

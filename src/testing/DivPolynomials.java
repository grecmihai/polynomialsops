package testing;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import models.Operatii;
import models.Polinom;

public class DivPolynomials {

	@Test
	public void test() {
		Polinom p1 = new Polinom("7x^5-3x^2+4");
		Polinom p2 = new Polinom("x^3-x^2+7x-19");
		List<Polinom> p3 = new ArrayList<Polinom>();
		p3 = Operatii.impartire(p1, p2);
		Polinom cat = p3.get(0);
		Polinom rest = p3.get(1);
		assertEquals("7.00x^2+7.00x-42.00" , cat.toString());
		assertEquals("39.00x^2+427.00x-794.00" , rest.toString());
	}

}

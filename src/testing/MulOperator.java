package testing;

import static org.junit.Assert.*;

import org.junit.Test;

import models.Operatii;
import models.Polinom;

public class MulOperator {

	@Test
	public void test() {
		Polinom p1 = new Polinom("x^2+x+1");
		Polinom p2 = new Polinom("x-1");
		Polinom p3 = Operatii.inmultire(p1, p2);
		assertEquals("x^3-1.00" , p3.toString());
	}

}

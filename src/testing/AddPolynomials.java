package testing;

import static org.junit.Assert.*;

import org.junit.Test;

import models.Operatii;
import models.Polinom;

public class AddPolynomials {

	@Test
	public void test() {
		Polinom p1 = new Polinom("x^3-3x^2+2x-4");
		Polinom p2 = new Polinom("3x^4+3x^2");
		Polinom p3 = Operatii.add(p1, p2);
		assertEquals("3.00x^4+x^3+2.00x-4.00" , p3.toString());
	}

}

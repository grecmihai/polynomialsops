package testing;

import static org.junit.Assert.*;

import org.junit.Test;

import models.Operatii;
import models.Polinom;

public class IntPolynomials {

	@Test
	public void test() {
		Polinom p1 = new Polinom("4x^3-x+12");
		Polinom p2 = Operatii.integrare(p1);
		assertEquals("x^4-0.50x^2+12.00x" , p2.toString());
	}

}

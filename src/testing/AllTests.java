package testing;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ AddPolynomials.class, DerPolynomials.class, DivPolynomials.class, IntPolynomials.class,
		MulOperator.class, SubPolynomials.class })
public class AllTests {

}

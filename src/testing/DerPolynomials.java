package testing;

import static org.junit.Assert.*;

import org.junit.Test;

import models.Operatii;
import models.Polinom;

public class DerPolynomials {

	@Test
	public void test() {
		Polinom p1 = new Polinom("x^3-3x^2+2x-4");
		Polinom p2 = Operatii.derivare(p1);
		assertEquals("3.00x^2-6.00x+2.00" , p2.toString());
	}

}

package testing;

import static org.junit.Assert.*;

import org.junit.Test;

import models.Operatii;
import models.Polinom;

public class SubPolynomials {

	@Test
	public void test() {
		Polinom p1 = new Polinom("7x^4-x^2-x+17");
		Polinom p2 = new Polinom("x^5-x^2+x");
		Polinom p3 = Operatii.sub(p1, p2);
		assertEquals("-x^5+7.00x^4-2.00x+17.00" , p3.toString());
	}

}

package controller;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import models.*;
import views.*;
public class PolinoameController {
	private PolinoameView polinoameView;
	public PolinoameController(PolinoameView view){
		polinoameView = view;
		//se adauga ascultatori tuturor butoanelor din fiecare fereastra
		polinoameView.getAdunarePage().addBAddListener(new BAddListener());
		polinoameView.getDerivarePage().addBDerListener(new BDerListener());
		polinoameView.getImpartirePage().addBImpListener(new BImpListener());
		polinoameView.getInmultirePage().addBInmListener(new BInmListener());
		polinoameView.getIntegrarePage().addBIntListener(new BIntListener());
		polinoameView.getMenuPage().addAdListener(new MenuListener());
		polinoameView.getMenuPage().addDerListener(new MenuListener());
		polinoameView.getMenuPage().addImpListener(new MenuListener());
		polinoameView.getMenuPage().addInmListener(new MenuListener());
		polinoameView.getMenuPage().addInteListener(new MenuListener());
		polinoameView.getMenuPage().addScListener(new MenuListener());
		polinoameView.getScaderePage().addBScListener(new BScListener());
		polinoameView.getAdunarePage().addBackListener(new AdunareBackListener());
		polinoameView.getScaderePage().addBackListener(new ScadereBackListener());
		polinoameView.getInmultirePage().addBackListener(new InmultireBackListener());
		polinoameView.getImpartirePage().addBackListener(new ImpartireBackListener());
		polinoameView.getDerivarePage().addBackListener(new DerivareBackListener());
		polinoameView.getIntegrarePage().addBackListener(new IntegrareBackListener());
	}
	class AdunareBackListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			polinoameView.getAdunarePage().setVisible(false);
			polinoameView.getMenuPage().setVisible(true);
		}
	}
	class ScadereBackListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			polinoameView.getScaderePage().setVisible(false);
			polinoameView.getMenuPage().setVisible(true);
		}
	}
	class InmultireBackListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			polinoameView.getInmultirePage().setVisible(false);
			polinoameView.getMenuPage().setVisible(true);
		}
	}
	class ImpartireBackListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			polinoameView.getImpartirePage().setVisible(false);
			polinoameView.getMenuPage().setVisible(true);
		}
	}
	class DerivareBackListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			polinoameView.getDerivarePage().setVisible(false);
			polinoameView.getMenuPage().setVisible(true);
		}
	}
	class IntegrareBackListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			polinoameView.getIntegrarePage().setVisible(false);
			polinoameView.getMenuPage().setVisible(true);
		}
	}
	class BAddListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			String s1,s2;
			//se preiau sub forma de String cele 2 polinoame introduse in casete
			s1 = polinoameView.getAdunarePage().getPol1().getText();
			s2 = polinoameView.getAdunarePage().getPol2().getText();
			//se verifica daca s'au introdus ambele polinoame
			if (!s1.isEmpty() && !s2.isEmpty()){
				Polinom p1 = new Polinom(s1);
				Polinom p2 = new Polinom(s2);
				//se executa operatia corespunzatoare
				Polinom p3 = models.Operatii.add(p1 , p2);
				polinoameView.getAdunarePage().getRezultat2().setText(p3.toString());
			}
			else
				JOptionPane.showMessageDialog(null, "Introduceti ambele polinoame" , "" , JOptionPane.PLAIN_MESSAGE);
		
		}
	}
	class BDerListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			//se preia sub forma de String cele 2 polinomul introdus in caseta
		String s1 = polinoameView.getDerivarePage().getPol1().getText();
		//se verifica daca a fost introdus polinomul
		if (!s1.isEmpty()){
			Polinom p1 = new Polinom(s1);
			//se executa operatia corespunzatoare
			Polinom p3 = models.Operatii.derivare(p1);
			polinoameView.getDerivarePage().getRezultat2().setText(p3.toString());
		}
		else
			JOptionPane.showMessageDialog(null, "Introduceti polinomul" , "" , JOptionPane.PLAIN_MESSAGE);
	
	}
	}
	class BImpListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			//se preiau sub forma de String cele 2 polinoame introduse in casete
			String s1 = polinoameView.getImpartirePage().getPol1().getText();
			String s2 = polinoameView.getImpartirePage().getPol2().getText();
			//se verifica daca s'au introdus ambele polinoame
			if (!s1.isEmpty() && !s2.isEmpty()){
				Polinom p1 = new Polinom(s1);
				Polinom p2 = new Polinom(s2);
				List<Polinom> p3 = new ArrayList<Polinom>();
				//se executa operatia corespunzatoare
				p3 = models.Operatii.impartire(p1, p2);
				Polinom p4 = p3.get(0);
				Polinom p5 = p3.get(1);
				polinoameView.getImpartirePage().getRezultat1().setText(p4.toString());
				polinoameView.getImpartirePage().getRezultat2().setText(p5.toString());
			}
			else
				JOptionPane.showMessageDialog(null, "Introduceti ambele polinoame" , "" , JOptionPane.PLAIN_MESSAGE);
		
		}
	}
	class BInmListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			//se preiau sub forma de String cele 2 polinoame introduse in casete
			String s1 = polinoameView.getInmultirePage().getPol1().getText();
			String s2 = polinoameView.getInmultirePage().getPol2().getText();
			//se verifica daca s'au introdus ambele polinoame
			if (!s1.isEmpty() && !s2.isEmpty()){
				Polinom p1 = new Polinom(s1);
				Polinom p2 = new Polinom(s2);
				//se executa operatia corespunzatoare
				Polinom p3 = models.Operatii.inmultire(p1 , p2);
				polinoameView.getInmultirePage().getRezultat2().setText(p3.toString());
			}
			else
				JOptionPane.showMessageDialog(null, "Introduceti ambele polinoame" , "" , JOptionPane.PLAIN_MESSAGE);
		
		}
	}
	class BIntListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			//se preia sub forma de String cele 2 polinomul introdus in caseta
		String s1 = polinoameView.getIntegrarePage().getPol1().getText();
		//se verifica daca a fost introdus polinomul
		if (!s1.isEmpty()){
			Polinom p1 = new Polinom(s1);
			//se executa operatia corespunzatoare
			Polinom p3 = models.Operatii.integrare(p1);
			polinoameView.getIntegrarePage().getRezultat2().setText(p3.toString());
		}
		else
			JOptionPane.showMessageDialog(null, "Introduceti polinomul" , "" , JOptionPane.PLAIN_MESSAGE);
	
	}
	}
	class BScListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			//se preia sub forma de String cele 2 polinomul introdus in caseta
			String s1 = polinoameView.getScaderePage().getPol1().getText();
			String s2 = polinoameView.getScaderePage().getPol2().getText();
			//se verifica daca a fost introdus polinomul
			if (!s1.isEmpty() && !s2.isEmpty()){
				Polinom p1 = new Polinom(s1);
				Polinom p2 = new Polinom(s2);
				//se executa operatia corespunzatoare
				Polinom p3 = models.Operatii.sub(p1 , p2);
				polinoameView.getScaderePage().getRezultat2().setText(p3.toString());
			}
			else
				JOptionPane.showMessageDialog(null, "Introduceti ambele polinoame" , "" , JOptionPane.PLAIN_MESSAGE);
		
		}
	}
	class MenuListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			if (event.getSource() == polinoameView.getMenuPage().getAd()){
				polinoameView.getMenuPage().setVisible(false);
				polinoameView.getAdunarePage().setVisible(true);
				polinoameView.getAdunarePage().getRezultat2().setText("");
				polinoameView.getAdunarePage().getPol1().setText("");
				polinoameView.getAdunarePage().getPol2().setText("");
			}
			else
				if (event.getSource() == polinoameView.getMenuPage().getSc()){
					polinoameView.getMenuPage().setVisible(false);
					polinoameView.getScaderePage().setVisible(true);
					polinoameView.getScaderePage().getRezultat2().setText("");
					polinoameView.getScaderePage().getPol1().setText("");
					polinoameView.getScaderePage().getPol2().setText("");
				}
				else
					if (event.getSource() == polinoameView.getMenuPage().getInm()){
						polinoameView.getMenuPage().setVisible(false);
						polinoameView.getInmultirePage().setVisible(true);
						polinoameView.getInmultirePage().getRezultat2().setText("");
						polinoameView.getInmultirePage().getPol1().setText("");
						polinoameView.getInmultirePage().getPol2().setText("");
					}
					else
						if(event.getSource() == polinoameView.getMenuPage().getImp()){
							polinoameView.getMenuPage().setVisible(false);
							polinoameView.getImpartirePage().setVisible(true);
							polinoameView.getImpartirePage().getRezultat1().setText("");
							polinoameView.getImpartirePage().getRezultat2().setText("");
							polinoameView.getImpartirePage().getPol1().setText("");
							polinoameView.getImpartirePage().getPol2().setText("");
						}
						else
							if(event.getSource() == polinoameView.getMenuPage().getDer()){
								polinoameView.getMenuPage().setVisible(false);
								polinoameView.getDerivarePage().setVisible(true);
								polinoameView.getDerivarePage().getRezultat2().setText("");
								polinoameView.getDerivarePage().getPol1().setText("");
							}
							else
								if(event.getSource() == polinoameView.getMenuPage().getInte()){
									polinoameView.getMenuPage().setVisible(false);
									polinoameView.getIntegrarePage().setVisible(true);
									polinoameView.getIntegrarePage().getRezultat2().setText("");
									polinoameView.getIntegrarePage().getPol1().setText("");
								}
			
		}
	}
}
